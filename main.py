import random
import time
import pygame


screen = pygame.display.set_mode((800, 600))

draw_on = False
last_pos = (0, 0)
color = (255, 128, 0)
radius = 1


def roundline(srf, color, start, end, radius=1):
    dx = end[0]-start[0]
    dy = end[1]-start[1]
    distance = max(abs(dx), abs(dy))
    for i in range(distance):
        x = int(start[0]+float(i)/distance*dx)
        y = int(start[1]+float(i)/distance*dy)
        pygame.draw.circle(srf, color, (x, y), radius)

try:
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                raise StopIteration
            if event.type == pygame.MOUSEBUTTONDOWN:
                color = (random.randrange(256), random.randrange(256), random.randrange(256))
                pygame.draw.circle(screen, color, event.pos, radius)
                draw_on = True
            if event.type == pygame.MOUSEBUTTONUP:
                draw_on = False
            if event.type == pygame.MOUSEMOTION:
                if draw_on:
                    pygame.draw.circle(screen, color, event.pos, radius)
                    roundline(screen, color,event.pos, last_pos,  radius)
                last_pos = event.pos
            if event.type == pygame.KEYDOWN :
                print event.key
                if event.key == 115:
                    pygame.image.save(screen, "./Data/"+time.strftime("%d.%m.%Y.%H.%M.%S")+".jpeg")
                if event.key == 99:
                    screen.fill((0,0,0))

        pygame.display.flip()

except StopIteration:
    pass

pygame.quit()
