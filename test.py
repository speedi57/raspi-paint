from pylab import imread
from stl_tools import numpy2stl
from scipy.misc import lena, imresize
from scipy.ndimage import gaussian_filter

A =imread("./Data/15.10.2014.22.18.21.jpeg")
A = A[:, :, 2] + 1.0*A[:,:, 0] # Compose RGBA channels to give depth
A = gaussian_filter(A, 1)
numpy2stl(A, "./Data/NASA.stl", scale=0.05, mask_val=1., solid=True)
